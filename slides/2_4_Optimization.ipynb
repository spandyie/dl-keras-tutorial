{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import pylab as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Deep Learning for Biomedicine with Keras\n",
    "## Optimization\n",
    "\n",
    "\n",
    "\n",
    "<img src=\"./images/segmentation.png\" width=\"30%\" align=\"right\">\n",
    "\n",
    "\n",
    "[*Walter de Back*](http://walter.deback.net)\n",
    "\n",
    "Institute for Medical Informatics and Biometry (IMB)  \n",
    "*\"Carl Gustav Carus\"* Faculty of Medicine  \n",
    "TU Dresden\n",
    "\n",
    "[GitLab repo](https://gitlab.com/wdeback/dl-keras-tutorial)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Training neural networks\n",
    "\n",
    "<img src=\"./images/neural_nets_learning.png\" width=\"60%\" align=\"right\">\n",
    "\n",
    "\n",
    "1. Forward pass\n",
    "2. Compute loss\n",
    "3. Back propagation\n",
    "4. **Optimization**\n",
    "\n",
    "<div class=\"reference\">\n",
    "Source: [Angermüller et al., Mol Sys Biol, 2016](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4965871/pdf/MSB-12-878.pdf)\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Optimizers\n",
    "\n",
    "<img src=\"./images/optimizers.gif\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Once we know the contribution of a weight to the loss, we'd like to update the weight to reduce the loss. \n",
    "\n",
    "- Methods:\n",
    "    - Stochastic gradient descent\n",
    "    - Momentum\n",
    "    - Nesterov\n",
    "    - RMSProp\n",
    "    - Adagrad\n",
    "    - Adam\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Mini batch SGD\n",
    "\n",
    "<img src=\"./images/sgd.png\" width=\"25%\" align=\"right\">\n",
    "\n",
    "1. Draw a batch of training samples `x` and corresponding targets `y`.\n",
    "2. Run the network on `x` to obtain predictions `y_pred`.\n",
    "3. Compute the loss of the network on the batch, a measure of the mismatch\n",
    "between `y_pred` and `y`.\n",
    "4. Compute the gradient of the loss with regard to the network’s parameters (a\n",
    "backward pass).\n",
    "5. Move the parameters a little in the opposite direction from the gradient—for\n",
    "example `W -= step * gradient` — thus reducing the loss on the batch a bit.\n",
    "\n",
    "\n",
    "Section 2.4.3 of [Chollet, F., 2018. Deep learning with Python. Manning Publications.](http://www.deeplearningitalia.com/wp-content/uploads/2017/12/Dropbox_Chollet.pdf)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Minibatch SGD\n",
    "\n",
    "- SGD \n",
    "  - draw 1 sample\n",
    "  - inaccurate but cheap\n",
    "- Batch SGD\n",
    "  - draw all samples\n",
    "  - accurate but expensive\n",
    "- Mini-batch SGD\n",
    "  - draw small batch (i.e. 4 or 128)\n",
    "  - efficient compromise\n",
    "  - size typically limited to GPU memory\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "## Minibatch SGD\n",
    "\n",
    "### Pseudo code\n",
    "```\n",
    "for i in range(nb_epochs=100):\n",
    "  data = np.random.shuffle(data)\n",
    "  for batch in get_batch(data, batch_size=128):\n",
    "    params_grad = evaluate_gradient(loss_function, batch, params)\n",
    "    params = params - learning_rate * params_grad\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## SGD\n",
    "\n",
    "<img src=\"./images/without_momentum.gif\" width=\"25%\" align=\"right\">\n",
    "\n",
    "Vanilla SGD:  \n",
    "$\\theta = \\theta_{t-1} - \\eta \\nabla_\\theta \\mathcal{L}(\\theta)$\n",
    "\n",
    "Minibatch SGD (batchsize $n$):  \n",
    "$\\theta = \\theta_{t-1} - \\eta \\nabla_\\theta \\mathcal{L}(\\theta; x_{i}^{i+n}; y_{i}^{i+n})$\n",
    "\n",
    "\n",
    "(+) Easy to write   \n",
    "(-) Difficult to set learning rate\n",
    "  - small: slow convergence\n",
    "  - large: overshoot, fluctuations\n",
    "\n",
    "[S. Ruder, blog](http://ruder.io/optimizing-gradient-descent/index.html#gradientdescentvariants)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## SGD with momentum\n",
    "\n",
    "<img src=\"./images/with_momentum.gif\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- [Momentum](http://linkinghub.elsevier.com/retrieve/pii/S0893608098001166) helps accelerate SGD in the relevant direction and dampens oscillations. \n",
    "- Adds a fraction $\\gamma$ of the update vector of the past time step to the current update vector.\n",
    "- Typically $\\gamma \\approx 0.95$\n",
    "\n",
    "SGD with momentum:  \n",
    "$v_t = \\gamma v_{t-1} + \\eta \\nabla_\\theta \\mathcal{L}(\\theta) \\\\\n",
    "\\theta = \\theta_{t-1} - v_t$\n",
    "\n",
    "\n",
    "(+) Faster convergence  \n",
    "(+) Less oscillations \n",
    "\n",
    "[S. Ruder, blog](http://ruder.io/optimizing-gradient-descent/index.html#gradientdescentvariants)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nesterov accelerated gradient (NAG)\n",
    "\n",
    "<img src=\"./images/nesterov.jpeg\" width=\"35%\" align=\"right\">\n",
    "\n",
    "- Anticipates next step based on previous update\n",
    "\n",
    "NAG:  \n",
    "$v_t = \\gamma v_{t-1} + \\eta \\nabla_\\theta \\mathcal{L}(\\theta -\\gamma v_{t−1}) \\\\\n",
    "\\theta = \\theta_{t-1} - v_t$\n",
    "\n",
    "- $\\theta -\\gamma v_{t−1}$ is predicted next position\n",
    "\n",
    "(+) prevents going too fast  \n",
    "(+) increases responsiveness\n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adagrad\n",
    "\n",
    "<img src=\"./images/nesterov.jpeg\" width=\"35%\" align=\"right\">\n",
    "\n",
    "- adapts the learning rate to the parameters:\n",
    "    - larger updates for infrequent\n",
    "    - smaller updates for frequent parameters\n",
    "- different $\\eta$ for each parameter $\\theta_i$\n",
    "\n",
    "SGD, update per parameter:  \n",
    "$\\theta_i = \\theta_{i, t-1} - \\eta g_{i,t-1}$  where $g_{i,t} = \\nabla_\\theta \\mathcal{L}(\\theta_i)$\n",
    "\n",
    "Adagrad, update per parameter:  \n",
    "$\\theta_i = \\theta_{i, t-1} - \\frac{\\eta}{\\sqrt{\\mathbf{G}_{ii, t-1} + \\epsilon}} g_{i,t-1}$  \n",
    "where $\\mathbf{G}$ is a diagonal matrix with sum of the squares of the gradients upto time $t$ and $\\epsilon$ is small smoothing value.\n",
    "\n",
    "\n",
    "(+) no need to tune learning rate  \n",
    "(+) well-suited for dealing with sparse data   \n",
    "(-) dimishing learning rates, as $\\mathbf{G}$ grows\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Adadelta and RMSProp\n",
    "\n",
    "- Based on Adagrad\n",
    "- Resolve diminishing learning rates\n",
    "\n",
    "- RMSProp:\n",
    "  - does not use sum of gradients in $\\mathbf{G}$\n",
    "  - instead uses exponentially decaying average of gradients: $v_t = 0.9 \\cdot g_{t-1}^2 + 0.1 \\cdot g_t^2$\n",
    "\n",
    "- Adadelta:\n",
    "  - also uses exponentially decaying average of gradients\n",
    "  - uses second moment of $v_t$ instead of learning rate $\\eta$\n",
    "  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Adaptive Moment Estimation (Adam) \n",
    "\n",
    "- Adaptive learning rates per parameter\n",
    "    - Exponentially decaying average of past square gradients (like RMSProp and Adadelta)\n",
    "    - Exponentially decaying average of past gradients (like momentum)\n",
    "\n",
    "\n",
    "$m_t = \\beta_1 m_{t-1} + (1- \\beta_1) g_t$   \n",
    "$v_t = \\beta_2 v_{t-1} + (1- \\beta_2) g_t^2$\n",
    "\n",
    "- First and second moments of gradient (mean and variance)\n",
    "\n",
    "Adam:  \n",
    "$\\theta_i = \\theta_{i, t-1} - \\frac{\\eta}{\\sqrt{\\hat v_t + \\epsilon}} \\hat m_{t}$   \n",
    "\n",
    "where $\\hat v$ and $\\hat m$ are bias-corrected moments (issue with first few steps).\n",
    "\n",
    "- Good default parameters: $\\beta_1 = 0.9$, $\\beta_2 = 0.999$, $\\epsilon = 1e^{-8}$\n",
    "\n",
    "(+) good default choice of optimizer  \n",
    "(+) little need for parameter tuning  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Optimizers\n",
    "\n",
    "<img src=\"./images/optimizers.gif\" width=\"25%\" align=\"right\">\n",
    "<img src=\"./images/optimizers2.gif\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- Methods\n",
    "    - Stochastic gradient descent\n",
    "    - Momentum\n",
    "    - Nesterov\n",
    "    - RMSProp\n",
    "    - Adagrad\n",
    "    - Adam\n",
    "\n",
    "- Bottom line:\n",
    "  - Use minibatches\n",
    "  - Take `Adam` as default optimizer \n",
    "  \n",
    "- More info:\n",
    "  - [S. Ruder's blog post](http://ruder.io/optimizing-gradient-descent)\n",
    "  - [A. Karpathy @ cs231n](http://cs231n.github.io/neural-networks-3/#update)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Next: Regularization\n",
    "\n",
    "<img src=\"./images/juhu.webp\" width=\"25%\" align=\"right\">\n",
    "\n",
    "- We can now optimize our model for our training data!\n",
    "- But how to make sure it is also good for our test data?\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
